# SehatQ Store - ReactJS App

This application built for Mobile-only with maximum screen width of 414px (iPhone 6/7/8 Plus as reference)

## Credentials

Sign In using username and password below :
- Username : sehatq
- Password : sehatq

## How to Run

- Open sehatq-store folder
- run 'npm install' from fresh clone
- then run 'npm start'
- make sure the app run on 'https://localhost:3000'

## HTTPS Required

This app using https for facebook sign in, if you're facing https error, please accept security risk in your browser

## Spesifications

- ReactJS v.16.12.0
- React Redux v.7.1.3
- Redux v.4.0.4