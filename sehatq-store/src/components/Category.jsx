import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  outerWrapper: {
    maxWidth: '100%',
    overflowX: 'scroll',
    margin: '16px 0'
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%'
  },
  image: {
    width: '90px',
    height: '90px',
    padding: '8px',
    background: '#FEFEFE'
  },
  thumbnail: {
    height: '100%'
  },
  title: {
    fontWeight: 'bold',
    padding: '8px',
    textAlign: 'center'
  },
  block: {
    width: '100px',
    display: 'inline-block'
  }
}));

const Category = (props) => {
  const classes = useStyles();
  const { categories } = props;

  return (
    <div className={classes.outerWrapper}>
      <div className={classes.wrapper}>
        {categories ?
          categories.map((category, index) => (
            <div key={index} className={classes.block}>
              <div className={classes.image}>
                <img src={category.imageUrl} alt={category.name} className={classes.thumbnail} />
              </div>

              <div className={classes.title}>
                {category.name}
              </div>
            </div>
          ))
          : null}
      </div>
    </div>
  );
};

export default Category;