import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  navigator: {
    position: 'relative',
    width: '100%',
    background: '#F9F9F9',
    borderTop: '1px solid #A1A1A1'
  },
  menu: {
    borderRight: '1px solid #F0F0F0',
    textAlign: 'center',
    padding: '8px 0',
    width: '24%',
    display: 'inline-block',
    cursor: 'pointer'
  },
  menuActive: {
    background: '#ffc107',
    color: '#fff',
    borderRight: '1px solid #F0F0F0',
    fontWeight: 'bold',
    textAlign: 'center',
    padding: '12px 0',
    width: '24%',
    display: 'inline-block',
    cursor: 'pointer'
  }
}));

const Navigator = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.navigator}>
      <div className={classes.menuActive}>
        Home
      </div>

      <div className={classes.menu}>
        Feed
      </div>

      <div className={classes.menu}>
        Cart
      </div>

      <div className={classes.menu}>
        Profile
      </div>
    </div>
  );
};

export default Navigator;