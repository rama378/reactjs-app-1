import React from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  product: {
    padding: '8px 0 16px 0'
  },
  productCard: {
    position: 'relative',
    border: '1px solid #F1F1F1',
    borderRadius: '16px',
    padding: '8px',
    margin: '16px'
  },
  image: {
    width: '80%',
    margin: '0 auto'
  },
  thumbnail: {
    width: '100%'
  },
  title: {
    fontWeight: 'bold',
    fontSize: '16px',
    background: '#70cbcf',
    color: '#fff',
    padding: '8px 16px',
    borderRadius: '0 0 16px 16px'
  }, 
  icon: {
    position: 'absolute',
    padding: '0 8px',
    bottom: '64px',
    left: '16px',
    color: '#fff',
    fontSize: '35px',
    background: '#F1F1F1',
    border: '1px solid #A1A1A1',
    borderRadius: '8px'
  }
}));

const Product = (props) => {
  const classes = useStyles();
  const { products } = props;

  return (
    <div className={classes.product}>
      {products ?
        products.map((product, index) => (
          <div key={index} className={classes.productCard}>
            <div className={classes.image}>
              <img src={product.imageUrl} alt={product.name} className={classes.thumbnail} />
            </div>

            <div className={classes.title}>
              {product.title}
            </div>

            <div className={classes.icon}>❤</div>
          </div>
        ))
        : null}
    </div>
  );
};

export default Product;