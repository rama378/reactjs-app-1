import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  header: {
    position: 'relative',
    width: '100%',
    background: '#F9F9F9',
    borderBottom: '1px solid #A1A1A1'
  },
  icon: {
    margin: '16px',
    width: '10%',
    display: 'inline-block',
    textAlign: 'center',
    fontSize: '40px',
    color: '#ffc107',
    cursor: 'pointer'
  },
  input: {
    margin: '16px',
    width: '70%',
    display: 'inline-block'
  }
}));

const Header = (props) => {
  const classes = useStyles();
  const { clickAction, handleChange, isSearchPage } = props;

  const [search, setSearch] = React.useState('');

  return (
    <div className={classes.header}>
      {
        isSearchPage ? 
        <div>
          <div className={classes.icon} onClick={clickAction}>←</div>
          <TextField
            value={search}
            onChange={(event) => { setSearch(event.target.value); handleChange(event.target.value); }}
            label="Search"
            variant="outlined"
            fullWidth
            autoFocus
            className={classes.input}
          />
        </div> :
        <div>
          <div className={classes.icon}>❤</div>
          <TextField
            value={search}
            onChange={(event) => handleChange(event.target.value)}
            onClick={clickAction}
            label="Search"
            variant="outlined"
            fullWidth
            className={classes.input}
          />
        </div>
      }
      
    </div>
  );
};

export default Header;