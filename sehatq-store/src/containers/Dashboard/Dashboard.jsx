import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core';
import Header from '../../components/Header';
import Category from '../../components/Category';
import Product from '../../components/Product';
import Navigator from '../../components/Navigator';

const useStyles = makeStyles(theme => ({
  fixedHeader: {
    position: 'fixed',
    top: 0,
    zIndex: 99,
    background: '#fff',
    width: '100%',
    maxWidth: '414px'
  },
  fixedNavigator: {
    position: 'fixed',
    bottom: 0,
    zIndex: 99,
    background: '#fff',
    width: '100%',
    maxWidth: '414px'
  },
  product: {
    marginTop: '310px',
    marginBottom: '32px'
  }
}));

const Dashboard = (props) => {
  const classes = useStyles();
  const { isLoggedIn, categories, productPromo, updateCategories, updateProductPromo, history } = props; 

  const [isInitialized, setIsInitialized] = React.useState(false);
  const [products, setProducts] = React.useState(null);

  React.useEffect(() => {
    if (!isInitialized && products) {
      setIsInitialized(true);
      updateCategories(products.data.category);
      updateProductPromo(products.data.productPromo);
    }
  });

  const openSearchPage = () => {
    history.push('/search');
  };

  const getCategories = () => {
    axios.get('https://private-4639ce-ecommerce56.apiary-mock.com/home').then(res => {
      const products = res.data;

      setProducts(products[0]);
    });
  };

  if (!isLoggedIn) {
    history.push('/login');
  }

  if (!products) {
    getCategories();
  }

  return (
    <div>
      <div className={classes.fixedHeader}>
        <Header clickAction={openSearchPage} handleChange={() => {}} />
        <Category categories={categories} />
      </div>

      <div className={classes.product}>
        <Product products={productPromo} />
      </div>

      <div className={classes.fixedNavigator}>
        <Navigator />
      </div>
    </div>
  );
};

export default Dashboard;