import { connect } from 'react-redux';
import Dashboard from './Dashboard';
import * as ACTIONS from '../../actions';

function mapStateToProps(state) {
  const { home, dashboard } = state;

  return {
    isLoggedIn: home.isLoggedIn,
    categories: dashboard.categories,
    productPromo: dashboard.productPromo
  };
}

const mapDispatchToProps = (dispatch) => ({
  setLoggedIn: () => {
    dispatch({ type: ACTIONS.LOGGED_IN });
  },
  updateCategories: (categories) => {
    dispatch({ type: ACTIONS.UPDATE_CATEGORY, categories });
  },
  updateProductPromo: (productPromo) => {
    dispatch({ type: ACTIONS.UPDATE_PRODUCT_PROMO, productPromo });
  }
});

const DashboardWithProps = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default DashboardWithProps;