import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Login from '../Login';
import Dashboard from '../Dashboard';
import Search from '../Search';
import { makeStyles } from '@material-ui/core';
import * as styles from './styles';

const Home = (props) => {
  const classes = makeStyles(theme => styles.default)();

  return (
    <div className={classes.main}>
      <Switch>
          <Redirect from="/" exact={true} to="/dashboard" />
          <Route path="/(dashboard)" exact={true} component={Dashboard} />
          <Route path="/(login)" exact={true} component={Login} />
          <Route path="/(search)" exact={true} component={Search} />
      </Switch>
    </div>
  );
}

export default Home;
