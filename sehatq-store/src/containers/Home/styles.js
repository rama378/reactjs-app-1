const styles = ({
    main: {
      width: '100%',
      maxWidth: '414px',
      height: '100%',
      minHeight: '700px',
      margin: '0 auto',
      background: '#FFFFFF'
    }
});

export default styles;