import { connect } from 'react-redux';
import * as ACTIONS from '../../actions';
import Login from './Login';

function mapStateToProps(state) {
  const { home } = state;

  return {
    isLoggedIn: home.isLoggedIn
  };
}

const mapDispatchToProps = (dispatch) => ({
  setLoggedIn: (value) => {
    dispatch({ type: ACTIONS.LOGGED_IN, value });
  }
});

const LoginWithProps = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default LoginWithProps;