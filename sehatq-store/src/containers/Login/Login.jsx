import React from 'react';
import { makeStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { useSnackbar } from 'notistack';
import * as styles from './styles';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';

const Login = (props) => {
  const classes = makeStyles(theme => styles.default)();
  const { enqueueSnackbar } = useSnackbar();

  const { isLoggedIn, setLoggedIn, history } = props; 
  
  const [rememberMe, setRememberMe] = React.useState(false);
  const [username, setUsername] = React.useState(localStorage.getItem('username') || '');
  const [password, setPassword] = React.useState(localStorage.getItem('password') || '');

  React.useEffect(() => {
    if (isLoggedIn) {
      enqueueSnackbar('Sign In successful', { variant: 'success' });
    }
  });

  const signIn = () => {
    authenticate();
  };

  const authenticate = () => {
    if (!username || username === '') {
      enqueueSnackbar('Username is required', { variant: 'error' });
    } else if (!password || password === '') {
      enqueueSnackbar('Password is required', { variant: 'error' });
    } else if (username !== 'sehatq' && password !== 'sehatq') {
      enqueueSnackbar('Username or password is incorrect', { variant: 'error' });
    } else {
      setLoggedIn(true);
      saveCredentials();
    }
  };

  const saveCredentials = () => {
    if (rememberMe) {
      localStorage.setItem('username', username);
      localStorage.setItem('password', password);
    }
  };

  const facebookCallback = (response) => {
    /* Hi! I don't have any facebook account, 
      when I create the App and set App ID, 
      and check the Off button to On,
      then trying to Sign In the result always
      "Aplikasi Tidak Tersedia: Aplikasi yang akan Anda gunakan sudah tidak ada atau aksesnya telah dibatasi."
      with response status undefined.

      To make demo running, let's assume the response status is undefined when it's success
    */

    console.log('Developer Note :', 'Lets assume this is a successful Facebook Sign In (View my code for more info)');

    if (typeof response.status === 'undefined') {
      setLoggedIn(true);
    }
  };

  const googleSuccessCallback = (response) => {
    setLoggedIn(true);
  };

  const googleFailureCallback = (response) => {
    enqueueSnackbar('Failed to Sign In with Google, please try again', { variant: 'error' });
  };

  if (isLoggedIn) {
    history.push('/dashboard');
  }

  return (
    <div className={classes.main}>
      <div className={classes.titleBlock}>
        <div className={classes.titleLogo}>
          SehatQ Store
        </div>
        <div className={classes.titleText}>
          LOGIN
        </div>
      </div>

      <div className={classes.login}>
        <TextField
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          onSubmit={signIn}
          label="Username"
          variant="outlined"
          fullWidth
          className={classes.input}
        />

        <TextField
          type="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          onSubmit={signIn}
          label="Password"
          variant="outlined"
          fullWidth
          className={classes.input}
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={rememberMe}
              onChange={() => setRememberMe(!rememberMe)}
              color="primary"
              className={classes.input}
            />
          }
          label="Remember Me"
        />

        <Button variant="contained" color="primary" className={classes.inputButton} onClick={signIn}>
          Sign In
        </Button>
      </div>

      <div className={classes.socmedBlock}>
        <FacebookLogin
          appId="2609096269209243"
          fields="name,email"
          callback={facebookCallback}
          textButton="SIGN IN WITH FACEBOOK"
          size="medium"
          className={classes.socmedButton}
        />
        
        <GoogleLogin
          clientId="85311331673-hgood8t1joc5rt5mkce442jrvnc5mbs0.apps.googleusercontent.com"
          buttonText="SIGN IN WITH GOOGLE"
          onSuccess={googleSuccessCallback}
          onFailure={googleFailureCallback}
          className={classes.socmedButton}
        />
      </div>
    </div>
  );
};

export default Login;