const styles = ({
    main: {
      padding: '30% 0'
    },
    titleBlock: {
      width: '80%',
      margin: '0 auto',
    },
    titleLogo: {
      fontSize: '24px',
      fontWeight: 'bold',
      color: '#ffc107'
    },
    titleText: {
      color: '#70cbcf'
    },
    login: {
      padding: '10%'
    },
    input: {
      margin: '16px 0'
    },
    inputButton: {
      margin: '16px 0',
      float: 'right'
    },
    socmedBlock: {
      width: '50%',
      margin: '16px auto'
    },
    socmedButton: {
      margin: '16px 0'
    }
});

export default styles;