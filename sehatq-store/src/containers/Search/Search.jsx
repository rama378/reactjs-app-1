import React from 'react';
import axios from 'axios';
import { makeStyles } from '@material-ui/core';
import Header from '../../components/Header';
import Product from '../../components/Product';

const useStyles = makeStyles(theme => ({
  fixedHeader: {
    position: 'fixed',
    top: 0,
    zIndex: 99,
    background: '#fff',
    width: '100%',
    maxWidth: '414px'
  },
  product: {
    marginTop: '80px',
    paddingBottom: '32px'
  }
}));

const Search = (props) => {
  const classes = useStyles();
  const { productPromo, history } = props; 

  const refProducts = React.useRef(productPromo);

  const [products, setProducts] = React.useState(refProducts.current);

  const openHomePage = () => {
    history.push('/dashboard');
  };

  const search = (query) => {
    let newProducts = null;

    if (query !== '') {
      for (let product of products) {
        if (Number(product.title.toLowerCase().search(query.toLowerCase()) >= 0)) {
          if (!newProducts) {
            newProducts = [];
          }

          newProducts.push(product);
        }
      }
    }

    setProducts(newProducts ? newProducts : products);
  }

  return (
    <div>
      <div className={classes.fixedHeader}>
        <Header clickAction={openHomePage} isSearchPage={true} handleChange={search} />
      </div>

      <div className={classes.product}>
        <Product products={products} />
      </div>
    </div>
  );
};

export default Search;