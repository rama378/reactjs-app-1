import { connect } from 'react-redux';
import Search from './Search';

function mapStateToProps(state) {
  const { dashboard } = state;

  return {
    productPromo: dashboard.productPromo
  };
}

const SearchWithProps = connect(
  mapStateToProps,
  null
)(Search);

export default SearchWithProps;