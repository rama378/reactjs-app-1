import React from 'react';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles';
import { SnackbarProvider } from 'notistack';
import store from './store';
import theme from './theme'
import Home from '../src/containers/Home';

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <SnackbarProvider maxSnack={1}>
        <Router>
            <Home />
        </Router>
      </SnackbarProvider>
    </ThemeProvider>
  </Provider>
);

export default App;
