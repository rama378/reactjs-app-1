import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#70cbcf",
      main: "#70cbcf",
      dark: "#70cbcf"
    },
    secondary: {
      light: "#ffc107",
      main: "#ffc107",
      dark: "#ffc107"
    }
  }
});

export default theme;
