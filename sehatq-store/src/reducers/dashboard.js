import Immutable from 'seamless-immutable';
import * as ACTIONS from '../actions';

const defaultState = Immutable({
  categories: [],
  productPromo: []
});

export const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.UPDATE_CATEGORY: {
      return state.merge({ categories: action.categories });
    }
    case ACTIONS.UPDATE_PRODUCT_PROMO: {
      return state.merge({ productPromo: action.productPromo });
    }
    default:
      return state;
  }
};

export default reducer;
