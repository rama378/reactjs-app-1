import Immutable from 'seamless-immutable';
import * as ACTIONS from '../actions';

const defaultState = Immutable({
  isLoggedIn: false
});

export const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.LOGGED_IN: {
      return state.merge({ isLoggedIn: action.value });
    }
    default:
      return state;
  }
};

export default reducer;
